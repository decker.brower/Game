"use strict"
var serve = require('koa-static'),
    router = require('koa-router')(),
    Jade = require('koa-jade'),
    koa = require('koa'),
    app = koa();

app.use(serve('node_modules/systemjs/dist'));
app.use(serve('public'));

let jade = new Jade({
  viewPath: 'views',
  debug: false,
  pretty: false,
  compileDebug: false,
  // locals: global_locals_for_all_pages,
  // basedir: '',
  // helperPath: [
  //   'path/to/jade/helpers',
  //   { random: 'path/to/lib/random.js' },
  //   { _: require('lodash') }
  // ],
  app: app // equals to jade.use(app) and app.use(jade.middleware)
});

router.get('/', function *() { this.render('index'); });
router.get('/debug', function *() { this.render('debug'); });

app.use(router.routes());

app.listen(1989);

console.log('listening on port 1989');

import {Sprite, KeysDown} from './Sprite';
import {Villain} from './Villain';
import {Goblin} from './Goblin';
import {Boots} from './Boots';
import {Canvas} from './Canvas';

export class Hero extends Sprite {
    constructor(canvas: Canvas) {
        super(canvas);
        this.image.src = '/images/Hero_Sprite.png';
    }

    move(modifier: number, keysDown: Object) {
        for(var key in keysDown) {
            switch(parseInt(key)) {
                case KeysDown.left:
                    this.x -= this.speed * modifier;

                    if(this.animIndex === 3)
                        this.animIndex = 0;

                    this.changeImage(1, this.animIndex);
                    this.animIndex += 1;
                    break;
                case KeysDown.right:
                    this.x += this.speed * modifier;

                    if(this.animIndex === 3)
                        this.animIndex = 0;

                    this.changeImage(2, this.animIndex);
                    this.animIndex += 1;
                    break;
                case KeysDown.up:
                    this.y -= this.speed * modifier;

                    this.x += this.vector.x;
                    this.y += this.vector.y;

                    if(this.animIndex === 3)
                        this.animIndex = 0;

                    this.changeImage(3, this.animIndex);
                    this.animIndex += 1;
                    break;
                case KeysDown.down:
                    this.y += this.speed * modifier;

                    if(this.animIndex === 3)
                        this.animIndex = 0;

                    this.changeImage(0, this.animIndex);
                    this.animIndex += 1;
                    break;
                default:
                    break;
            }
        };

        //if Hero leaves the canvas wrap him around to the other side
        if(this.x > this.canvas.width)
            this.x = -32;

        if(this.y > this.canvas.height)
            this.y = -32;

        if(this.x < -32)
            this.x = this.canvas.width;

        if(this.y < -32)
            this.y = this.canvas.height;
    }

    collide(gameObj: Sprite) {
        if(gameObj instanceof Goblin) {
                gameObj.destroy();
                //set score
        }

        if(gameObj instanceof Villain) {
            // reset game
        }

        if(gameObj instanceof Boots) {
            gameObj.destroy();
            this.speedUp(50);
        }
    }

    reset() {
        this.speed = 256;
        this.x = (this.canvas.width / 2) - (this.width / 2);
        this.y = (this.canvas.height / 2) - (this.height / 2);
    }
}

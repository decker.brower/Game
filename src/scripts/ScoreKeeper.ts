export class ScoreKeeper {
    points: number
    goblinsCaught: number

    constructor() {
        this.reset();
    }

    goblinCaught() {
        this.goblinsCaught += 1;
    }

    addPoints(points: number) {
        this.points += points;
    }

    reset() {
        this.points = 0;
        this.goblinsCaught = 0;
    }
}

import {Sprite} from './Sprite';
import {Hero} from './Hero';
import {Villain} from './Villain';
import {Goblin} from './Goblin';
import {Boots} from './Boots';
import {SoundManager} from './SoundManager';
import {ScoreKeeper} from './ScoreKeeper';
import {Canvas} from './Canvas';

export interface keyEvent {
    keyCode: number
}
export class Game {
    Canvas: Canvas
    SoundManager: SoundManager
    ScoreKeeper: ScoreKeeper
    Hero: Hero
    Villains: Array<Villain>
    Boots: Boots
    Goblins: Array<Goblin>
    gameObjs: Array<any>
    keysDown: Object
    backgroundImg: any
    now: number
    then: number
    delta: number
    mainInterval: any

    constructor() {
        this.Canvas = new Canvas({width: 1024, height: 912});
        this.SoundManager = new SoundManager();
        this.ScoreKeeper = new ScoreKeeper();
        this.Hero = new Hero(this.Canvas);
        this.Boots = new Boots(this.Canvas);
        this.backgroundImg = new Image();
        this.backgroundImg.src = 'images/background.png';
        this.keysDown = { left: false, right: false, up: false, down: false}

        this.buildGameObjs();

        addEventListener('keydown', (e: keyEvent) => {
            this.keysDown[e.keyCode] = e.keyCode
        });

        addEventListener('keyup', (e: keyEvent) => {
            delete this.keysDown[e.keyCode];
        });

        document.body.appendChild(this.Canvas.el);

        this.play();
    }

    render() {
        if(this.backgroundImg.complete)
            this.Canvas.drawBackground(this.backgroundImg);

        this.gameObjs.forEach((obj) => {
            if(obj.image.complete)
                this.Canvas.drawSprite(obj);
        });

        this.Canvas.drawText({
            color: "rgb( 250, 250, 250 )",
			font: "18px cursive",
			align: "left",
			baseLine: "top",
			text: `Score: ${this.ScoreKeeper.points}`
        }, 395, 32);

        this.Canvas.drawText({
            color: "rgb( 250, 250, 250 )",
			font: "18px cursive",
			align: "left",
			baseLine: "top",
			text: `Goblins caught: ${this.ScoreKeeper.goblinsCaught}`
        }, 32, 32);
    }

    step(modifier: number) {
        this.gameObjs.forEach((obj, index) => {
            if(obj.remove) {
                this.gameObjs.splice(index, 1);
            }

            obj.move(modifier);
        });

        this.checkForCollisions();
    }

    checkForCollisions() {
        var gameObj1,
            gameObj2,
            gameObjsLength = this.gameObjs.length;

        for(var n = 0; n < gameObjsLength; n++) {
            gameObj1 = this.gameObjs[n];

            for(var i = 0; i < gameObjsLength; i++) {
                gameObj2 = this.gameObjs[i];

                if(
                    gameObj1.x <= (gameObj2.x + gameObj2.width) &&
                    gameObj2.x <= (gameObj1.x + gameObj1.width) &&
                    gameObj1.y <= (gameObj2.y + gameObj2.height) &&
                    gameObj2.y <= (gameObj1.y + gameObj1.height)
                ) {
                    gameObj1.collide(gameObj2);
                }
            }
        }
    }

    addGoblin() {
        this.gameObjs.push(new Goblin(this.Canvas));
    }

    addVillain() {
        this.gameObjs.push(new Villain(this.Canvas));
    }

    buildGameObjs() {
        this.gameObjs = [
            this.Hero,
            this.Boots,
            new Villain(this.Canvas),
            new Goblin(this.Canvas)
        ]
    }

    mainLoop(timestamp) {
        this.now = timestamp;
        requestAnimationFrame(this.mainLoop);
        this.delta = this.now - this.then;
        this.render();
        this.step(this.delta / 1600);
        this.render();
        this.then = this.now;

    }

    play() {
        this.then = Date.now();
        this.mainInterval = setInterval(() => { this.mainLoop(); }, 1);
        requestAnimationFrame(this.mainLoop);
    }

    reset() {
        this.ScoreKeeper.reset();
        this.SoundManager.reset();
        this.Canvas.reset();
        this.Hero.reset();
        this.Boots.reset();
        this.buildGameObjs();
    }
}

import {Canvas} from './Canvas';
import {Sprite} from './Sprite';

export class Boots extends Sprite {
    constructor(canvas: Canvas) {
        super(canvas);
        this.image.src = '/images/boots.png';
        this.reset();
    }

    reset() {
        this.x = 32 + (Math.random() * this.canvas.width - 64);
        this.y = 32 + (Math.random() * this.canvas.width - 64);
    }
}

export class SoundManager {
    disabled: boolean
    goblinCaught: any
    speedBoost: any
    evilLaugh: any
    backgroundMusic: any
    ready: boolean

    constructor() {
        this.disabled = true;
		this.goblinCaught = new Audio('/sounds/goblin_caught.wav');
		this.speedBoost = new Audio('/sounds/speedboost.wav');
		this.evilLaugh = new Audio('/sounds/evil_laugh.mp3');
		this.backgroundMusic = new Audio('/sounds/music.mp3');

        this.backgroundMusic.addEventListener('ended', this.reset());

		this.backgroundMusic.onload = () => {
			this.ready = true;
		};
    }

    reset() {
        if (this.disabled)
            return false;

        this.backgroundMusic.currentTime = 0;
        this.backgroundMusic.play();
    }
}

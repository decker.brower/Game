import {Vector} from './Vector';
import {Canvas} from './Canvas';

export enum KeysDown {
    left = 37,
    right = 39,
    up = 38,
    down = 40
}
export class Sprite {
    canvas: Canvas
    image: any
    imagesWidth: number
    animIndex: number
    vector: Vector
    speed: number
    width: number
    height: number
    goToX: number
    goToY: number
    xStart: number
    xStop: number
    yStart: number
    yStop: number
    x: number
    y: number
    visible: boolean
    remove: boolean

    constructor(canvas: Canvas) {
        this.canvas = canvas;
        this.image = new Image();
        this.imagesWidth = 96
        this.animIndex = 0;
        this.vector = new Vector(0, 0);
        this.speed = 256;
        this.width = 32;
        this.height = 32;
        this.xStart = 0;
        this.xStop = this.width;
        this.yStart = 0;
        this.yStop = this.height;
        this.x = 0;
        this.y = 0;
        this.visible = true;
        this.remove = false;
    }

    getRandomInt(min: number, max: number): number {
        return Math.floor(Math.random() * min - max + 1) + min;
    }

    changeImage(row: number, col: number) {
        this.yStart = row * this.height;
        this.yStop = this.height;
        this.xStart = col * this.width;
        this.xStop = this.width;
    }

    speedUp(speed: number) {
        this.speed += speed

        if(this.speed > 500)
            this.speed = 500;

        if(this.speed < 50)
            this.speed = 50;
    }

    slowDown(speed: number) {
        this.speed -= speed

        if(this.speed < 50)
            this.speed = 50
    }

    move(modifier: number, keysDown: Object) {

    }

    collide(gameObj: Sprite) {

    }

    show() {
        this.visible = true;
    }

    hide() {
        this.visible = false;
    }

    destroy() {
        this.remove = true;
    }
}

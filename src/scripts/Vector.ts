export class Vector {
    x: number
    y: number

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    scale(s: number) {
        this.x *= s
        this.y *= s
    }

    add(vector: Vector) {
        this.x += vector.x
        this.y += vector.y
    }

    subtract(vector: Vector) {
        this.x -= vector.x
        this.y -= vector.y
    }

    // point the vector in opposite direction
    negate(vector: Vector) {
        this.x = -this.x
        this.y = -this.y
    }

    length(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    //a faster length calculation that returns the length squared.
	//usefull if all you want to know is that one vector is longer than the other
    lengthSquared(): number {
        return this.x * this.x + this.y * this.y;
    }

    //change vector into unit length vector pointing in same direction
    normalize(): number {
        var l = this.length();

        if (l) {
            this.x /= l
            this.y /= l
        }

        return length;
    }

    rotate(angle: number) {
        var x = this.x,
            y = this.y,
            cosVal = Math.cos(angle),
            sinVal = Math.sin(angle);

        this.x = x * cosVal - y * sinVal;
        this.y = x * sinVal + y * cosVal;
    }

    toString(): string {
        return `( ${this.x.toFixed(3)}, ${this.y.toFixed(3)} )`;
    }
}

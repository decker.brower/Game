import {Vector} from './Vector';
import {Sprite} from './Sprite';
import {Hero} from './Hero';
import {Canvas} from './Canvas';

export class Goblin extends Sprite {

    constructor(canvas: Canvas) {
        super(canvas);
        this.image.src = '/images/Catgirl_Sprite.png';

        this.reset();
    }

    reset() {
        this.vector = new Vector(0, 0);
        this.goToX = this.getRandomInt(0, this.canvas.width);
        this.goToY = this.getRandomInt(0, this.canvas.height);
        this.x = 32 + (Math.random() * this.canvas.width - 64);
        this.y = 32 + (Math.random() * this.canvas.width - 64);
    }

    move(modifier) {
        var v = new Vector(this.goToX - this.x, this.goToY - this.y);
        v.normalize();

        this.vector.add(v);
        this.vector.scale(modifier);
        this.x += this.vector.x;
        this.y += this.vector.y;
    }

    collide(gameObj: Sprite) {
        if(gameObj instanceof Hero) {
            this.reset();
        }

    }
}

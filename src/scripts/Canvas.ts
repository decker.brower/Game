import {Sprite} from './Sprite';

export interface TextParams {
    color: string
    font: string
    align: string
    baseLine: string
    text: string
}
export interface CanvasParams {
    width: number
    height: number
}
export class Canvas {
    el: any
    ctx: any
    width: number
    height: number

    constructor(params: CanvasParams) {
        this.el = document.createElement('canvas');
        this.ctx = this.el.getContext('2d');
        this.width = params.width;
        this.height = params.height;
        this.el.width = this.width;
        this.el.height = this.height;
    }

    drawSprite(sprite: Sprite) {
        this.ctx.drawImage(
                sprite.image,
                sprite.xStart,
                sprite.yStart,
                sprite.xStop,
                sprite.yStop,
                sprite.x,
                sprite.y,
                sprite.width,
                sprite.height
        );
    }

    drawBackground(img: any) {
        this.ctx.drawImage(img, 0, 0, this.el.width, this.el.height);
    }

    drawText(params: TextParams, x: number, y: number) {
        this.ctx.fillstyle = params.color;
        this.ctx.font = params.font;
        this.ctx.textAlign = params.align;
        this.ctx.textBaseline = params.baseLine;
        this.ctx.fillText(params.text, x, y);
    }

    reset() {
        // Store the current transformation matrix
        this.ctx.save()
        // Use the identity matrix while clearing the canvas
        this.ctx.setTransform(1, 0, 0, 1, 0, 0);
        this.ctx.clearRect(0, 0, this.width, this.height);

        // Restore the transform
        this.ctx.restore();
    }
}

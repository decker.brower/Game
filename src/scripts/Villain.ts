import {Canvas} from './Canvas';
import {Sprite} from './Sprite';

export class Villain extends Sprite {
    constructor(canvas: Canvas) {
        super(canvas);
        this.image.src = '/images/Ninja_Sprite.png';
    }

    move(modifier) {

    }
}

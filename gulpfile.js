var gulp = require('gulp'),
    clean = require('gulp-clean'),
    uglify = require('gulp-uglifyjs'),
    ts = require('gulp-typescript');

gulp.task('clean-build', function() {
    return gulp.src('public/js', {read: false})
        .pipe(clean());
});

gulp.task('typescript-dev', function() {
    return gulp.src('src/scripts/**/*.ts')
        .pipe(ts({
            module: 'system',
            noImplicitAny: false
        }))
        .pipe(gulp.dest('public/js'));
});

gulp.task('typescript-production', function() {
    return gulp.src('src/scripts/**/*.ts')
        .pipe(ts({
            module: 'system',
            noImplicitAny: false
        }))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

gulp.task('build', ['clean-build', 'typescript-dev']);

gulp.task('default', ['clean-build', 'typescript-production']);
